import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import '../scss/Tile.css';

class Tile extends Component {

    render() {
        return (
            <Link to={this.props.to} className="tile">
                <img src={this.props.img} alt={this.props.text} />
                {this.props.text}
            </Link>
        );
    }

}

Tile.propTypes = {
    to: Link.propTypes.to,
    text: PropTypes.string.isRequired
};

export default Tile;