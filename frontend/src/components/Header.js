import React, { Component } from 'react';
import PropTypes from 'prop-types';
import logo from '../img/dollar-logo.png';
import {Link} from "react-router-dom";

class Header extends Component {

    constructor(props) {
        super(props);

        const closeBtn = this.props.closeBtn !== undefined && this.props.closeBtn;
        this.state = {
          closeBtn: closeBtn
        };
    }

    render() {
        return (
            <header>
                <div className="container flex">
                    <div className="appLogo">
                        <img src={logo} alt="Logo"/> <h1>Notowania walut</h1>
                    </div>
                    <h2>{this.props.subtitle}</h2>
                </div>
                { this.state.closeBtn ? <Link to="/" className="closeBtn" title="Zamknij okno">&times;</Link> : null }
            </header>
        );
    }

}

Header.propTypes = {
  subtitle: PropTypes.string.isRequired,
  closeBtn: PropTypes.bool
};

export default Header;