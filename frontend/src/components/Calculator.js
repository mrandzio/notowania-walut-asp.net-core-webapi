import React, { Component } from 'react';
import Header from "./Header";
import '../scss/Calculator.css';
import axios from 'axios'

class Calculator extends Component {

    constructor(props) {
        super(props);

        this.state = {
            sourceCurrency: 'USD',
            targetCurrency: 'PLN',
            date: new Date().toDateString(),
            resume: 0,
            currencies: [],
        };

        this.changeSourceCurrency = this.changeSourceCurrency.bind(this);
        this.changeTargetCurrency = this.changeTargetCurrency.bind(this);
        this.changeValue = this.changeValue.bind(this);
        this.createSelectItems = this.createSelectItems.bind(this);
    }

    componentWillMount() {
        document.title = "Notowania walut - kalkulator";
        axios.get('http://localhost:53506/currency/names')
            .then((response) => {
                this.setState({currencies: response.data});
            })
            .catch((error) => {
                console.log(error);
            });
    }

    createSelectItems() {
        let items = [];
        let currencies = this.state.currencies;
        for (let key in currencies) {
            if (currencies.hasOwnProperty(key)) {
                items.push (
                    <option key={key} value={key} style={{backgroundImage: `url(${require('../img/flags/' + key.toLowerCase() + '.png')})`}}>
                        {key} ({currencies[key]})
                    </option>
                );
            }
        }
        return items.sort(function(a, b){
            if(a.props.children[0] < b.props.children[0]) return -1;
            if(a.props.children[0] > b.props.children[0]) return 1;
            return 0;
        });
    }

    changeValue(source, target) {
        const sc = source !== undefined ? source : this.state.sourceCurrency;
        const tc = target !== undefined ? target : this.state.targetCurrency;
        const value = document.getElementById('amount').value;

        if (value === '') {
            this.setState({resume: 0});
        } else {
            axios.get('http://localhost:53506/currency/convert?from=' + sc + '&to=' + tc
                + '&amount=' + value + '&date=' + this.state.date)
                .then((response) => {
                    this.setState({resume: parseFloat(response.data)});
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }

    changeSourceCurrency(event) {
        const source = event.target.value.toUpperCase();
        this.setState({ sourceCurrency: source });
        this.changeValue(source);
    }

    changeTargetCurrency(event) {
        const target = event.target.value.toUpperCase();
        this.setState({ targetCurrency: target });
        this.changeValue(undefined, target);
    }

    render() {
        const selectItems = this.createSelectItems();

        return (
            <React.Fragment>
                <Header subtitle="Kalkulator" closeBtn={true} />
                <p className="container info">Wybierz walutę źródłową i docelową,
                    a następnie wprowadź kwotę, aby dokonać konwersji między jednostkami monetarnymi.</p>
                <div className="container flex select">
                    <div>
                        <h3>Waluta źródłowa:</h3>
                        <select size="3" value={this.state.sourceCurrency} onChange={ this.changeSourceCurrency }>
                            { selectItems }
                        </select>
                    </div>
                    <div>
                        <h3>Waluta docelowa:</h3>
                        <select size="3" value={this.state.targetCurrency} onChange={ this.changeTargetCurrency }>
                            { selectItems }
                        </select>
                    </div>
                </div>
                <div className="container box">
                    <label htmlFor="amount">Kwota:</label>
                    <input type="number" step="0.01" onChange={() => { this.changeValue() } } className="amount" id="amount" placeholder="Wpisz kwotę, która ma zostać przeliczona" />
                    <em>{ this.state.sourceCurrency }</em>
                </div>
                <div className="container" id="result">
                    Wynik: <em>{ this.state.resume } { this.state.targetCurrency }</em>
                </div>
            </React.Fragment>
        );
    }

}

export default Calculator;