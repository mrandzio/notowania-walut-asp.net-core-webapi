import React, { Component } from "react";
import Tile from "./Tile";
import '../scss/Home.css';
import tableIcon from '../img/table-icon.png';
import calculatorIcon from '../img/calculator-icon.png';
import chartIcon from '../img/chart-icon.png';
import Currency from "./Currency";
import Header from "./Header";
import axios from 'axios';

class Home extends Component {

    static progress;

    constructor(props) {
        super(props);

        this.state = {
            allCurrencies: [],
            visibleCurrencies: [],
            names: [],
            listOfProviders: [],
            currentProvider: '',
            currentIndex: 0
        };

        this.setProgress = this.setProgress.bind(this);
        this.setProvider = this.setProvider.bind(this);
    }

    componentWillMount() {
        document.title = "Notowania walut - strona główna";
        Home.progress = 0;

        this.getNames = this.makeCancelable(axios.get('http://localhost:53506/currency/names'));
        this.getNames.promise
            .then((response) => {
                this.setState({ names: response.data});
            })
            .catch((error) => {
                console.log(error);
            });

        this.getCurrencies = this.makeCancelable(axios.get('http://localhost:53506/currency/allbydate?Date=' + new Date().toDateString()));
        this.getCurrencies.promise
            .then((response) => {
                const c = response.data;
                this.setState({ allCurrencies: c, visibleCurrencies: c.slice(this.state.currentIndex, this.state.currentIndex + 4) });
                this.setProgress();
            })
            .catch((error) => {
                console.log(error);
            });

        this.getListOfProviders = this.makeCancelable(axios.get('http://localhost:53506/provider/all'));
        this.getListOfProviders.promise
            .then((response) => {
                this.setState({ listOfProviders: response.data });
            })
            .catch((error) => {
                console.log(error);
            });

        axios.get('http://localhost:53506/provider')
            .then((response) => {
                this.setState({ currentProvider: response.data });
            })
            .catch((error) => {
                console.log(error);
            })
    }

    makeCancelable(promise) {
        let hasCanceled_ = false;

        const wrappedPromise = new Promise((resolve, reject) => {
            promise.then(
                val => hasCanceled_ ? reject({isCanceled: true}) : resolve(val),
                error => hasCanceled_ ? reject({isCanceled: true}) : reject(error)
            );
        });

        return {
            promise: wrappedPromise,
            cancel() {
                hasCanceled_ = true;
            },
        };
    }

    setProgress() {
        this.progressInterval = setInterval(() => {
                document.getElementById('progressBar').style.width = Home.progress + '%';
                Home.progress += 0.5;

        }, 40);

        this.progressResetInterval = setInterval(() => {
            Home.progress = 0;

            const c = this.state.allCurrencies;
            const i = this.state.currentIndex >= 12 ? 0 : this.state.currentIndex + 4;
            this.setState({ visibleCurrencies: c.slice(i, i + 4), currentIndex: i });
        }, 8000);
    }

    setProvider(name) {
        axios.post('http://localhost:53506/provider', { ProviderName: name })
            .then(() => {
                this.setState({ currentProvider: name });
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            });
    }

    componentWillUnmount() {
        clearInterval(this.progressInterval);
        clearInterval(this.progressResetInterval);
        this.getNames.cancel();
        this.getCurrencies.cancel();
        this.getListOfProviders.cancel();
    }

    render() {
        const currencies = this.state.visibleCurrencies;
        const names = this.state.names;
        const listOfProviders = this.state.listOfProviders;
        const currentProvider = this.state.currentProvider;

        return (
            <React.Fragment>
                <Header subtitle="Strona główna" />
                <div className="container flex">
                    { currencies.length > 0 ?
                        <h3>Data notowań: <time>{ currencies[0].date }</time></h3>
                    : null }
                    <div className="progressContainer">
                        <div className="progressBar" id="progressBar" />
                    </div>
                </div>
                { currencies.length > 0 ?
                    <section className="LastRates container">
                        { currencies.map((item, index) => {
                            return <Currency key={index}
                                             shortName={item.symbol}
                                             fullName={names[item.symbol]}
                                             price={item.value}
                                             price7DaysEarlier={item.valueSevenDaysEarlier} />
                        })}
                    </section>
                : <div className="loader" style={{marginTop: '50px'}} /> }
                <nav className="Tiles">
                    <div>
                        <Tile to="/notowania" text="Notowania" img={tableIcon} />
                        <Tile to="/kalkulator" text="Kalkulator" img={calculatorIcon} />
                        <Tile to="/wykres" text="Wykres" img={chartIcon} />
                    </div>
                </nav>
                <section className="changeApi">
                    <div className="changeApiBody">
                        <h3>Wybierz API</h3>
                        <ul>
                            { listOfProviders.map((item, index) => {
                                return ( currentProvider === item ?
                                    <li key={index} className="active">{item}</li>
                                    : <li key={index} onClick={() => { this.setProvider(item) }}>{item}</li>)
                            }) }
                        </ul>
                    </div>
                    <button><i className="fas fa-cog" /></button>
                </section>
            </React.Fragment>
        );
    }

}

export default Home;