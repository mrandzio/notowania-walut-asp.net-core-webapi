import React, { Component } from 'react';
import Header from "./Header";
import '../scss/Rates.css';
import Currency from "./Currency";
import CurrencyRates from "./CurrencyRates";
import Helpers from "../Helpers";
import axios from 'axios';

class Rates extends Component {

    constructor(props) {
        super(props);

        this.state = {
            currencies: [],
            names: [],
            currency: '',
            date: '',
            showRates: false,
            showLoader: false,
            invalidDate: false
        };

        this.getRates = this.getRates.bind(this);
        this.showCurrencyRates = this.showCurrencyRates.bind(this);
        this.hideCurrencyRates = this.hideCurrencyRates.bind(this);
    }

    componentWillMount() {
        document.title = "Notowania walut - notowania";

        axios.get('http://localhost:53506/currency/names')
            .then((response) => {
                this.setState({ names: response.data});
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getRates(event) {
        const showRates = event.target.value !== '';

        if(showRates) {
            const date = new Date(event.target.value);

            if(date > new Date() || date < new Date('2000-01-01')) {
                this.setState({ invalidDate: true });
                return;
            }

            this.setState({ date: event.target.value, showLoader: true, invalidDate: false });
            axios.get('http://localhost:53506/currency/allbydate?Date=' + event.target.value)
                .then((response) => {
                    document.getElementById('date').blur();
                    this.setState({ currencies: response.data, showRates: showRates, showLoader: false });
                })
                .catch((error) => {
                    console.log(error);
                })
        } else {
            this.setState({ currencies: [], showRates: false, invalidDate: false });
        }
    }

    showCurrencyRates(currencyShortName) {
        document.body.className = 'blur';
        this.setState({ currency: currencyShortName, showChartWindow: true });
    }

    hideCurrencyRates() {
        document.body.className = '';
        this.setState({ showChartWindow: false });
    }

    render() {
        const currencies = this.state.currencies;
        const names = this.state.names;

        return (
            <React.Fragment>
                { this.state.showChartWindow ? <CurrencyRates currency={ this.state.currency } endDate={this.state.date} close={ this.hideCurrencyRates } /> : null }
                <Header subtitle="Notowania" closeBtn={true} />
                <p className="container info">
                    Określ datę, aby zobaczyć notowania walut z danego dnia.
                </p>
                <div className="container box" style={{position: 'relative'}}>
                    <label htmlFor="amount"><i className="far fa-calendar-alt fa-fw" /> Data:</label>
                    <input type="date" max={Helpers.getCurrentDate()} className="date" id="date" onChange={ this.getRates } />
                    { this.state.showLoader ? <div style={{position: 'absolute', right: '10%'}}><div className="loader" /></div> : null }
                    { this.state.invalidDate ? <div className="invalidDate">Błędna data</div> : null }
                </div>
                { this.state.showRates ?
                    <section className="Rates container">
                        { currencies.map((item, index) => {
                            return <Currency key={index}
                                        shortName={item.symbol}
                                        fullName={names[item.symbol]}
                                        price={item.value}
                                        price7DaysEarlier={item.valueSevenDaysEarlier} onClick={() => {
                                            this.showCurrencyRates(item.symbol)
                                        }} />
                        })}
                    </section>
                    : ( this.state.showLoader ?
                        <p className="noResult">Trwa pobieranie, proszę czekać...</p> :
                        <p className="noResult">(Nie określono daty)</p>
                    )
                }
            </React.Fragment>
        );
    }

}

export default Rates;