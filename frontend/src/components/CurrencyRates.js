import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../scss/CurrencyRates.css';
import axios from 'axios';
import Helpers from "../Helpers";

class CurrencyRates extends Component {

    constructor(props) {
        super(props);

        this.state = {
            list: [],
            showLoader: true
        }
    }

    componentWillMount() {
        const endDate = this.props.endDate;
        const startDate= new Date(endDate);
        startDate.setDate(startDate.getDate() - 6);

        axios.get('http://localhost:53506/currency/onebydaterange?Symbol=' + this.props.currency + '&StartDate=' + startDate.toDateString() + '&EndDate=' + endDate)
            .then((response) => {
                this.setState({ list: response.data.reverse(), showLoader: false });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    downloadFile(type) {
        const format = type === 'JSON' ? 'json' : 'xml';
        const downloadUrl = 'http://localhost:53506/quotes/download?file=';
        const list = this.state.list;

        axios.post('http://localhost:53506/quotes/exporttofile', {
            Symbol: list[0].symbol,
            Data: list,
            FileType: format
        })
            .then((response) => {
                window.location.href = downloadUrl + response.data;
            })
            .catch((error) => {
                console.log(error);
            });
    }

    render() {
        const list = this.state.list;

        return (
            <div className="window" role="dialog">
                <section>
                    <header>
                        <h3>Waluta { this.props.currency } - notowania</h3>
                        <button onClick={ this.props.close }>&times;</button>
                    </header>
                    { this.state.showLoader ? <div className="loader" style={{marginTop: '40px'}} /> : null }
                    <ul>
                        { list.map((item, index) => {
                            return <li key={index}>{Helpers.formatPrice(item.value)} <time><i className="far fa-calendar-alt fa-fw" /> {item.date}</time></li>
                        })}
                    </ul>
                    { this.state.showLoader ? null :
                    <div className="buttons">
                        <button onClick={() => { this.downloadFile('JSON') }}>Eksportuj do JSON</button>
                        <button onClick={() => { this.downloadFile('XML') }}>Eksportuj do XML</button>
                    </div> }
                </section>
            </div>
        );
    }

}

CurrencyRates.propTypes = {
    currency: PropTypes.string.isRequired,
    close: PropTypes.func.isRequired
};

export default CurrencyRates;