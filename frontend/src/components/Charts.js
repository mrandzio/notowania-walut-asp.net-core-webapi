import React, { Component } from 'react';
import Header from "./Header";
import '../scss/Charts.css';
import Chart from 'chart.js/dist/Chart.bundle';
import Helpers from "../Helpers";
import axios from "axios/index";

class Charts extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activeTab: 0,
            currencies: ['USD', 'NONE', 'NONE'],
            currenciesSelectItem: [],
            datasets: [],
            label: [],
            median: false,
            average: false,
            date: new Date().toDateString(),
            invalidDate: false,
            colors: ['rgba(255,99,132,1)', 'rgba(215, 157, 44, 1)','rgba(84, 107, 189, 1)',
                'rgba(84, 160, 40, 0.9)','rgba(183, 0, 0, 1)']
    };

        this.changeTab = this.changeTab.bind(this);
        this.selectCurrency = this.selectCurrency.bind(this);
        this.createSelectItems = this.createSelectItems.bind(this);
        this.showChart = this.showChart.bind(this);
        this.setDate = this.setDate.bind(this);
        this.setAverage = this.setAverage.bind(this);
        this.setMedian = this.setMedian.bind(this);
        this.printChart = this.printChart.bind(this);
    }

    componentWillMount() {
        document.title = "Notowania walut - wykresy";
        axios.get('http://localhost:53506/currency/names')
            .then((response) => {
                this.setState({currenciesSelectItem: response.data});
            })
            .catch((error) => {
                console.log(error);
            });
    }

    componentDidMount() {
        //this.printChart();
    }

    printChart() {
        let ctx = document.getElementById("chart");
        new Chart(ctx, {
            type: 'line',
            data: {
                labels: this.state.label,
                datasets: this.state.datasets
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: false
                        }
                    }]
                }
            }
        });
        document.getElementById("loadChart").style.visibility = "hidden";
    }

    changeTab(num) {
        this.setState({activeTab: num});
    }

    showChart() {
        if (this.state.date !== '' && !this.state.invalidDate){
            let ctx = document.getElementById("chart");
            new Chart (ctx, {});
            document.getElementById("loadChart").style.visibility = "visible";
            document.getElementById("warningDate").style.visibility = "hidden";
            let currencies = this.state.currencies;
            let symbols = '';
            currencies = currencies.filter(function(elem, index, self) {
                return index === self.indexOf(elem);
            });
            currencies.forEach(function (item) {
                if (item !== 'NONE')
                    symbols += 'Symbols='+item+"&"
            });
            console.log('WithAverage='+ this.state.average.toString() +'&WithMedian='+ this.state.median.toString());
            axios.get('http://localhost:53506/currency/manybydaterangetochart?'+ symbols +'Date='+ this.state.date +'&WithAverage='+ this.state.average.toString() +'&WithMedian='+ this.state.median.toString())
                .then((response) => {
                    let currencies = response.data;
                    let datasets = [];
                    let label = currencies['dates'];
                    let colors = this.state.colors;
                    currencies['currencies'].forEach (function (item, index){
                        datasets.push({
                            label: item,
                            data: currencies['values'][index],
                            backgroundColor: ['transparent'],
                            borderColor: colors[index],
                            borderWidth: 2
                        });
                    });
                    this.setState({datasets: datasets, label: label});
                    this.printChart();
                })
                .catch((error) => {
                    console.log(error);
                });
        }
        else {
            document.getElementById("warningDate").style.visibility = "visible";
        }
    }

    selectCurrency(event) {
        let currencies = Object.assign([], this.state.currencies);
        currencies[this.state.activeTab] = event.target.value;
        this.setState({currencies: currencies});
    }

    createSelectItems() {
        let items = [];
        let currencies = this.state.currenciesSelectItem;
        for (var key in currencies) {
            if (key !== 'PLN')
            {
                items.push(<option key={key} value={key}
                                   style={{backgroundImage: `url(${require('../img/flags/' + key.toLowerCase() + '.png')})`}}>{key} ({currencies[key]})</option>);
            }
        }
        return items.sort(function(a, b){
            if(a.props.children[0] < b.props.children[0]) return -1;
            if(a.props.children[0] > b.props.children[0]) return 1;
            return 0;
        });
    }

    setDate(event) {
        const date = new Date(event.target.value);

        if(date > new Date() || date < new Date('2000-01-01')) {
            this.setState({ invalidDate: true });
        } else {
            this.setState({ date: event.target.value, invalidDate: false });
        }
    }

    setAverage(event) {
        this.setState((prevState) => { return { average: !prevState.average } });
    }

    setMedian(event) {
        this.setState((prevState) => { return { median: !prevState.median } });
    }

    render() {
        const date = Helpers.getCurrentDate();
        const selectItems = this.createSelectItems();

        return (
            <React.Fragment>
                <Header subtitle="Wykres" closeBtn={true} />
                <main className="flex">
                    <aside>
                        <div className="box chartCurrencies">
                            <ul className="tabs">
                                <li className={ this.state.activeTab === 0 ? 'active' : null } onClick={() => {this.changeTab(0)}}>Waluta 1</li>
                                <li className={ this.state.activeTab === 1 ? 'active' : null } onClick={() => {this.changeTab(1)}}>Waluta 2</li>
                                <li className={ this.state.activeTab === 2 ? 'active' : null } onClick={() => {this.changeTab(2)}}>Waluta 3</li>
                            </ul>
                            <select size="3" value={this.state.currencies[this.state.activeTab]} onChange={ this.selectCurrency }>
                                { this.state.activeTab !== 0 ? <option value="NONE">Brak waluty</option> : null }
                                { selectItems }
                            </select>
                        </div>
                        <div className="box" style={{position: 'relative'}}>
                            <label htmlFor="amount"><i className="far fa-calendar-alt fa-fw" /> Data:</label>
                            <input type="date" className="date" onChange={ this.setDate } defaultValue={date} max={date} />
                            { this.state.invalidDate ? <div className="invalidDate">Błędna data</div> : null }
                            <p id="warningDate" className="warning">!</p>
                        </div>
                        <div className="options">
                            <input onChange={ this.setAverage } type="checkbox" id="avg" />
                            <label htmlFor="avg">Pokaż średnią</label>
                            <input onChange={ this.setMedian } type="checkbox" id="median" />
                            <label htmlFor="median">Pokaż medianę</label>
                        </div>
                        <button className="generate" onClick={ this.showChart }>WYGENERUJ</button>
                    </aside>
                    <div className="chartContainer">
                        <div className="chartHoverFix" />
                        <canvas id="chart" />
                        <div id="loadChart" className="loader" style={{visibility: 'hidden'}} />
                    </div>
                </main>
            </React.Fragment>
        );
    }

}

export default Charts;
