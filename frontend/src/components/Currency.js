import React , { Component } from 'react';
import PropTypes from 'prop-types';
import '../scss/Currency.css';
import Helpers from "../Helpers";

class Variant {

    constructor(color, icon) {
        this.color = color;
        this.icon = icon;
    }

}

class Currency extends Component {

    constructor(props) {
        super(props);

        this.preparePrices = this.preparePrices.bind(this);
    }

    preparePrices() {
        const absoluteDiff = this.props.price - this.props.price7DaysEarlier;
        let variant;

        if(absoluteDiff > 0)
            variant = new Variant('green', 'fas fa-caret-up');
        else if(absoluteDiff < 0)
            variant = new Variant('red', 'fas fa-caret-down');
        else
            variant = new Variant('blue', 'fas fa-minus');

        return {
            price: Helpers.formatPrice(this.props.price),
            absoluteDiff: Helpers.formatPrice(absoluteDiff),
            percentDiff: Helpers.formatPercent(absoluteDiff / this.props.price),
            variant: variant
        };
    }

    render() {
        const clickable = this.props.onClick !== undefined;
        const prices = this.preparePrices();

        return (
          <article className="Currency"
                   style={clickable ? { cursor: 'pointer' } : null }
                   onClick={() => { if(clickable) this.props.onClick(); }}>
              <img src={require(`../img/flags/${this.props.shortName.toLowerCase()}.png`)} alt={ this.props.fullName } />
              <h4>{ this.props.shortName } &nbsp; <span>({ this.props.fullName })</span></h4>
              <div>{ prices.price }</div>
              <div className={ prices.variant.color }><i className={ prices.variant.icon } /> { prices.percentDiff }</div>
              <div className={ prices.variant.color }><i className={ prices.variant.icon } /> { prices.absoluteDiff }</div>
          </article>
        );
    }

}

Currency.propTypes = {
    shortName: PropTypes.string.isRequired,
    fullName: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    price7DaysEarlier: PropTypes.number.isRequired,
    onClick: PropTypes.func
};

export default Currency;
