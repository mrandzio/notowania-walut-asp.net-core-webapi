import React from 'react';

const Footer = () => {

    return (
        <footer>
            <strong>&copy; { new Date().getFullYear() }</strong> Mateusz Chwaszczewski, Mateusz Randzio, Kamil Ulezło
        </footer>
    );

};

export default Footer;