import React from 'react';
import ReactDOM from 'react-dom';
import './scss/index.css';
import App from './App';
import axios from 'axios';
import registerServiceWorker from './registerServiceWorker';

axios.defaults.withCredentials = true;

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
