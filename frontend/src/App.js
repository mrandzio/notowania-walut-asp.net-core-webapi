import React, { Component } from 'react';
import './scss/App.css';
import Home from "./components/Home";
import Rates from "./components/Rates";
import Calculator from "./components/Calculator";
import Charts from "./components/Charts";
import { BrowserRouter as Router, Route} from 'react-router-dom';
import Footer from "./components/Footer";

class App extends Component {

    render() {
        return (
            <Router>
                <div className="App">
                    <Route exact path="/" component={Home} />
                    <Route path="/notowania" component={Rates} />
                    <Route path="/kalkulator" component={Calculator} />
                    <Route path="/wykres" component={Charts} />
                    <Footer />
                </div>
            </Router>
        );
    }
}

export default App;
