class Helpers {

    static getCurrentDate() {
        let d = new Date();
        d.setMinutes(d.getMinutes() - d.getTimezoneOffset());
        return d.toJSON().slice(0,10);
    }

    static formatPrice(price) {
        return price.toFixed(4).replace('.', ',') + ' PLN';
    }

    static formatPercent(price) {
        return price.toFixed(4).replace('.', ',') + '%';
    }

}

export default Helpers;