﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CurrencyExchange.Migrations
{
    public partial class DeleteDifferences : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PercentDifference",
                table: "Currencies");

            migrationBuilder.RenameColumn(
                name: "PriceDifference",
                table: "Currencies",
                newName: "ValueSevenDaysEarlier");

            migrationBuilder.AlterColumn<string>(
                name: "Provider",
                table: "Currencies",
                nullable: true,
                oldClrType: typeof(string));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ValueSevenDaysEarlier",
                table: "Currencies",
                newName: "PriceDifference");

            migrationBuilder.AlterColumn<string>(
                name: "Provider",
                table: "Currencies",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "PercentDifference",
                table: "Currencies",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
