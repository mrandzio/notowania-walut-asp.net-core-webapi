﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace CurrencyExchange.Models
{
    public class Currency
    {
        [JsonIgnore]
        public int CurrencyId { get; set; }
        [Required]
        public string Base { get; set; }
        [Required]
        public string Symbol { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Required]
        public decimal Value { get; set; }
        [Required]
        public decimal ValueSevenDaysEarlier  { get; set; }
        [Required]
        [JsonIgnore]
        public string Provider { get; set; }
    }
}
