﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyExchange.Models.Binding
{
    public class SetProvider
    {
        [Required]
        public string ProviderName { get; set; }
    }
}
