﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyExchange.Models.Binding
{
    public class CurrenciesByDateRangeToChart
    {
        [Required]
        public string[] Symbols { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Required]
        public bool WithAverage { get; set; }
        [Required]
        public bool WithMedian { get; set; }

    }
}
