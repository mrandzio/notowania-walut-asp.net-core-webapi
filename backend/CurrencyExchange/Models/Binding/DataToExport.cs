﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyExchange.Models.Binding
{
    public class RecordItem
    {
        public string Base { get; set; }
        public string Symbol { get; set; }
        public string Date { get; set; }
        public string Value { get; set; }
        public string ValueSevenDaysEarlier { get; set; }
    }

    public class DataToExport
    {
        [Required]
        public string Symbol { get; set; }

        [Required]
        public RecordItem[] Data { get; set; }

        [Required]
        public string FileType { get; set; }
    }
}
