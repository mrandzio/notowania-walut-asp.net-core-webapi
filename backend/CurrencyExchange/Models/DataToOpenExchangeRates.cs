﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyExchange.Models
{
    public class CurrencyRatesTimeSeries
    {
        public string Disclaimer { get; set; }
        public string License { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Base { get; set; }
        public Dictionary<string, Dictionary<string, decimal>> Rates { get; set; }
    }
    public class CurrencyRates
    {
        public string Disclaimer { get; set; }
        public string License { get; set; }
        public int TimeStamp { get; set; }
        public string Base { get; set; }
        public Dictionary<string, decimal> Rates { get; set; }
    }
    public class ConvertOER
    {
        public string Disclaimer { get; set; }
        public string License { get; set; }
        public Dictionary<string, object> Request { get; set; }
        public Dictionary<string, decimal> Meta { get; set; }
        public decimal Response { get; set; }
    }


}
