﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurrencyExchange.Models.Binding;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.IO;

namespace CurrencyExchange.Controllers
{
    public class ProviderController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession Session => _httpContextAccessor.HttpContext.Session;
        private IEnumerable<ModelError> ModelStateErrors => ModelState.Values.SelectMany(v => v.Errors);

        private string[] Providers => new string[]
        {
            "fixer.io",
            "OpenExchangeRates"
        };

        public ProviderController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var providerName = Session.GetString("Provider");

            if (string.IsNullOrEmpty(providerName))
                providerName = "fixer.io";

            return Ok(providerName);
        }

        [HttpPost]
        public IActionResult Index([FromBody]SetProvider model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelStateErrors);
            if (!IsValidProvider(model.ProviderName))
                return BadRequest(new {message = "No valid provider."});
            Session.SetString("Provider", model.ProviderName);
            return Ok(new {message = "The new provider has been successfully set." });
        }

        private bool IsValidProvider(string providerName)
        {
            return providerName != null && Providers.Any(providerName.Contains);
        }

        [HttpGet]
        public IActionResult All()
        {
            return Ok(Providers);
        }

    }
}