﻿using System;
using CurrencyExchange.Models.Binding;
using CurrencyExchange.Services.ExchangeRateProviders;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;
using Convert = CurrencyExchange.Models.Binding.Convert;


namespace CurrencyExchange.Controllers
{
    public class CurrencyController : Controller
    {
        private IEnumerable<ModelError> ModelStateErrors => ModelState.Values.SelectMany(v => v.Errors);
        private readonly ExchangeRateProvider _exchangeRateProvider;

        public CurrencyController(ExchangeRateProvider exchangeRateProvider)
        {
            _exchangeRateProvider = exchangeRateProvider;
        }

        public IEnumerable<string> Available()
        {
            return _exchangeRateProvider.GetAvailableCurrencies;
        }

        [HttpGet]
        public IActionResult Index(GetCurrency model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelStateErrors);
            if (!IsValidCurrency(model.Symbol))
                return BadRequest(new {message = "No valid currency."});

            return Ok(_exchangeRateProvider.GetCurrency(model.Symbol, model.Date));
        }

        [HttpGet]
        public IActionResult OneByDateRange(CurrencyByDateRange model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelStateErrors);
            if (!IsValidCurrency(model.Symbol))
                return BadRequest(new { message = "No valid currency." });

            return Ok(_exchangeRateProvider.GetCurrencyByDateRange(model.Symbol, model.StartDate, model.EndDate));
        }

        [HttpPost]
        public IActionResult ManyByDate([FromBody] CurrenciesByDate model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelStateErrors);
            foreach (var symbol in model.Symbols)
            {
                if (!IsValidCurrency(symbol))
                    return BadRequest(new {message = symbol + " - no valid currency."});
            }

            return Ok(_exchangeRateProvider.GetCurrenciesByDate(model.Symbols, model.Date));
        }

        [HttpPost]
        public IActionResult ManyByDateRange([FromBody] CurrenciesByDateRange model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelStateErrors);
            foreach (var symbol in model.Symbols)
            {
                if (!IsValidCurrency(symbol))
                    return BadRequest(new { message = symbol + " - no valid currency." });
            }

            return Ok(_exchangeRateProvider.GetCurrenciesByDateRange(model.Symbols, model.StartDate, model.EndDate));
        }

        [HttpGet]
        public IActionResult ManyByDateRangeToChart(CurrenciesByDateRangeToChart model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelStateErrors);
            foreach (var symbol in model.Symbols)
            {
                if (!IsValidCurrency(symbol))
                    return BadRequest(new { message = symbol + " - no valid currency." });
            }

            Chart chart = new DefaultChart(_exchangeRateProvider.GetCurrenciesByDateRange(model.Symbols, model.Date.AddDays(-7), model.Date), model.Symbols);

            if (model.WithAverage)
                chart = new AverageOnChart(chart);
            if (model.WithMedian)
                chart = new MedianOnChart(chart);

            return Ok(chart.returnDataToChart());
        }

        [HttpGet]
        public IActionResult Convert(Convert model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelStateErrors);
            if (model.From == model.To)
                return Ok(model.Amount);
            if (!IsValidCurrency(model.From) || !IsValidCurrency(model.To))
                return BadRequest(new {message = "No valid currencies."});

            return Ok(_exchangeRateProvider.Convert(model.From, model.To, model.Amount, model.Date));
        }

        [HttpGet]
        public IActionResult AllByDate(DateTime date)
        {
            return Ok(_exchangeRateProvider.GetCurrenciesByDate(_exchangeRateProvider.GetAvailableCurrencies, date));
        }

        [HttpGet]
        public IActionResult Names()
        {
            return Ok(_exchangeRateProvider.GetCurrenciesNames);
        }

        private bool IsValidCurrency(string currency)
        {
            return currency != null && _exchangeRateProvider.GetAvailableCurrencies.Any(currency.Contains);
        }
    }
}
