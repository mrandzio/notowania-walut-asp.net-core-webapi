﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CurrencyExchange.Models.Binding;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Filters;
using CurrencyExchange.Services.ExportStrategies;
using System;
using Microsoft.AspNetCore.Http;

namespace CurrencyExchange.Controllers
{
    public class DeleteFileAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Body.Flush();

            var fileResult = filterContext.Result as FileResult;
            if (fileResult != null)
            {
                string filePath = "temp/" + fileResult.FileDownloadName;
                System.IO.File.Delete(filePath);
            }
        }
    }

    public class QuotesController : Controller
    {
        private IEnumerable<ModelError> ModelStateErrors => ModelState.Values.SelectMany(v => v.Errors);
        private IExport strategy { get; set; }
        private const string dir = "temp";

        [HttpPost]
        public IActionResult ExportToFile([FromBody] DataToExport model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelStateErrors);

            switch(model.FileType)
            {
                case "json":
                    strategy = new ExportToJson();
                    break;
                case "xml":
                    strategy = new ExportToXml();
                    break;
                default:
                    return BadRequest("Unrecognized file type.");
            }

            try
            {
                strategy.exportToFile(model, dir);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok(strategy.filename);
        }

        [HttpGet]
        [DeleteFileAttribute]
        public IActionResult Download(string file)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes($"{dir}/{file}");
            string ext = file.Substring(file.Length - 4);
            string contentType = ext.Equals("json") ? "application/json" : "text/xml";

            return File(fileBytes, contentType, file);
        }
    }
}