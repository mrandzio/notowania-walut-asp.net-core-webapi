﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyExchange
{
    public abstract class Chart
    {
        public abstract DataToChart returnDataToChart();
    }
}
