﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyExchange
{
    public class MedianOnChart : ChartDecorator
    {
        public MedianOnChart(Chart chart)
            : base(chart)
        {

        }

        public override DataToChart returnDataToChart()
        {
            DataToChart newDataToChart = base.returnDataToChart();

            int isAverage = newDataToChart.currencies.IndexOf("Średnia");
            int n = newDataToChart.currencies.Count;
            if (isAverage != -1)
                n--;
            n = n * newDataToChart.values[0].Count;

            newDataToChart.currencies.Add("Mediana");

            List<decimal> medians = new List<decimal>();
            List<decimal> list = new List<decimal>();

            int avg = isAverage == -1 ? 0 : -1;

            for (int i = 0; i < (newDataToChart.values.Count + avg); i++)
            {
                foreach (var item in newDataToChart.values[i])
                {
                    list.Add(item);
                }
            }

            list.Sort();

            decimal median;
            if (n % 2 == 1)
            {
                median = list[n / 2];
            }
            else
            {
                median = (list[n / 2] + list[(n / 2) - 1]) / 2;
            }
            for (int i = 0; i < newDataToChart.values[0].Count; i++)
            {
                medians.Add(median);
            }

            newDataToChart.values.Add(medians);

            return newDataToChart;
        }
    }
}
