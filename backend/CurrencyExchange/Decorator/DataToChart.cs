﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyExchange
{
    public class DataToChart
    {
        public List<string> dates { get; set; }
        public List<List<decimal>> values { get; set; }
        public List<string> currencies { get; set; }

        public DataToChart()
        {
            dates = new List<string>();
            values = new List<List<decimal>>();
            currencies = new List<string>();
        }
    }
}
