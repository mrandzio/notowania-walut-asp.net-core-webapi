﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyExchange
{
    public class ChartDecorator : Chart
    {
        protected Chart chart;
        public ChartDecorator (Chart chart)
        {
            this.chart = chart;
        }
        public override DataToChart returnDataToChart()
        {
            return chart.returnDataToChart();
        }
    }
}
