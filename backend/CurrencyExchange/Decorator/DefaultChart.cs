﻿using CurrencyExchange.Data;
using CurrencyExchange.Models;
using CurrencyExchange.Services.ExchangeRateProviders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyExchange
{
    public class DefaultChart : Chart
    {

        IDictionary<DateTime, IEnumerable<Currency>> dictionary;
        string[] symbols;
        public DefaultChart(IDictionary<DateTime, IEnumerable<Currency>> dictionary, string[] symbols)
        {
            this.dictionary = dictionary;
            this.symbols = symbols;
        }

        public override DataToChart returnDataToChart()
        {
            DataToChart myReturnData = new DataToChart();

            myReturnData.currencies = symbols.ToList();
            foreach(var currency in myReturnData.currencies)
            {
                myReturnData.values.Add(new List<decimal>());
            }

            foreach (KeyValuePair<DateTime, IEnumerable<Currency>> entry in dictionary)
            {
                myReturnData.dates.Add(entry.Key.ToString("d.MM.yyyy"));
                int i = 0;
                foreach (var currency in entry.Value)
                {
                    myReturnData.values[i++].Add(currency.Value);
                }
            }

            return myReturnData;
        }
    }
}
