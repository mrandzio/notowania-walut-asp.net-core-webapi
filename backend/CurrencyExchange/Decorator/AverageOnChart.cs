﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyExchange
{
    public class AverageOnChart : ChartDecorator
    {
        public AverageOnChart (Chart chart) 
            : base (chart)
        {

        }

        public override DataToChart returnDataToChart ()
        {
            DataToChart newDataToChart = base.returnDataToChart();
            newDataToChart.currencies.Add("Średnia");

            List<decimal> averages = new List<decimal>();
            decimal average = 0;
            for (int i = 0; i < newDataToChart.values.Count; i++)
            {
                    foreach (var item in newDataToChart.values[i])
                    {
                        average += item;
                    }
            }

            average = average / (newDataToChart.values.Count * newDataToChart.values[0].Count);

            for (int i = 0; i < newDataToChart.values[0].Count; i++)
            {
                    averages.Add(average);
            }
            newDataToChart.values.Add(averages);

            return newDataToChart;
        }


    }
}
