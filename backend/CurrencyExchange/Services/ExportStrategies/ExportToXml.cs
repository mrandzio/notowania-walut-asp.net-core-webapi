﻿using System;
using CurrencyExchange.Models.Binding;
using System.Xml;

namespace CurrencyExchange.Services.ExportStrategies
{
    public class ExportToXml : IExport
    {
        public string filename { get; private set; }

        public void exportToFile(DataToExport model, string path)
        {
            filename = $"notowania-{model.Symbol}-{DateTime.Now.ToString("yyyyMMddHHmmssf")}.xml";
            string filepath = $"{path}/{filename}";

            XmlDocument doc = new XmlDocument();
            XmlNode quotes = doc.CreateNode(XmlNodeType.Element, "quotes", null);

            var currencyAttr = doc.CreateAttribute("Currency");
            currencyAttr.Value = model.Symbol;

            var baseAttr = doc.CreateAttribute("Base");
            baseAttr.Value = model.Data[0].Base;

            quotes.Attributes.Append(currencyAttr);
            quotes.Attributes.Append(baseAttr);
            doc.AppendChild(quotes);

            foreach (RecordItem item in model.Data)
            {
                XmlNode record = doc.CreateNode(XmlNodeType.Element, "record", null);
                XmlNode date = doc.CreateNode(XmlNodeType.Element, "date", null);
                date.InnerText = item.Date;
                XmlNode price = doc.CreateNode(XmlNodeType.Element, "price", null);
                price.InnerText = item.Value;
                XmlNode price7daysEarlier = doc.CreateNode(XmlNodeType.Element, "priceSevenDaysEarlier", null);
                price7daysEarlier.InnerText = item.Value;

                record.AppendChild(date);
                record.AppendChild(price);
                record.AppendChild(price7daysEarlier);
                quotes.AppendChild(record);
            }

            doc.Save(filepath);
        }
    }
}
