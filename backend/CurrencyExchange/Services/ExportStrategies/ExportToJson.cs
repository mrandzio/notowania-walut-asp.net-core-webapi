﻿using CurrencyExchange.Models.Binding;
using Newtonsoft.Json;
using System;
using Microsoft.AspNetCore.Http;

namespace CurrencyExchange.Services.ExportStrategies
{
    public class ExportToJson : IExport
    {
        public string filename { get; private set; }

        public void exportToFile(DataToExport model, string path)
        {
            filename = $"notowania-{model.Symbol}-{DateTime.Now.ToString("yyyyMMddHHmmssf")}.json";

            string data = JsonConvert.SerializeObject(model.Data, Formatting.Indented);
            string filepath = $"{path}/{filename}";
            
            System.IO.File.WriteAllText(filepath, data);
        }
    }
}
