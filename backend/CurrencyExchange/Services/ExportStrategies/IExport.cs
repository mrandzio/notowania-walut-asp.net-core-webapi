﻿using CurrencyExchange.Models.Binding;

namespace CurrencyExchange.Services.ExportStrategies
{
    interface IExport
    {
        string filename { get; }
        void exportToFile(DataToExport model, string path);
    }
}
