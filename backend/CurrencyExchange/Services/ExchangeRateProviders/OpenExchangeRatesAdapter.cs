﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurrencyExchange.Models;

namespace CurrencyExchange.Services.ExchangeRateProviders
{
    public class OpenExchangeRatesAdapter : ExchangeRateProvider
    {
        private const string FromSymbol = "PLN";

        public override string ProviderName { get; } = "OpenExchangeRates";

        public override Currency GetCurrency(string currencyString, DateTime date)
        {
            CurrencyRates currencyRates = OpenExchangeRates.getHistoricalExchange(date);
            CurrencyRates differenceCurrencyRates = OpenExchangeRates.getHistoricalExchange(date.AddDays(-7));

            List<Currency> currencies = new List<Currency>();

            decimal USD = 0;
            decimal oldUSD = 0;

            for (int i = 0; i < GetAvailableCurrencies.Length; i++)
            {
                var symbol = GetAvailableCurrencies[i];

                if (symbol == "PLN")
                {
                    USD = currencyRates.Rates[GetAvailableCurrencies[i]];
                    oldUSD = differenceCurrencyRates.Rates[GetAvailableCurrencies[i]];
                }
            }

            for (int i = 0; i < GetAvailableCurrencies.Length; i++)
            {
                var symbol = GetAvailableCurrencies[i];

                if (symbol == currencyString)
                {
                    var current = USD / currencyRates.Rates[GetAvailableCurrencies[i]];
                    var difference = oldUSD / differenceCurrencyRates.Rates[GetAvailableCurrencies[i]];
                    var percent = (current - difference) / difference * 100;
                    return new Currency
                    {
                        Base = "PLN",
                        Symbol = symbol,
                        Date = date,
                        Value = decimal.Round(current, 6),
                        ValueSevenDaysEarlier = decimal.Round(difference, 6),
                        Provider = ProviderName
                    };
                }
            }
            return null;
        }


        public override IEnumerable<Currency> GetCurrenciesByDate(string[] currenciesSymbols, DateTime date)
        {
            CurrencyRates currencyRates = OpenExchangeRates.getHistoricalExchange(date);
            CurrencyRates differenceCurrencyRates = OpenExchangeRates.getHistoricalExchange(date.AddDays(-7));

            List<Currency> currencies = new List<Currency>();

            decimal USD = 0;
            decimal oldUSD = 0;

            for (int i = 0; i < GetAvailableCurrencies.Length; i++)
            {
                var symbol = GetAvailableCurrencies[i];

                if (symbol == "PLN")
                {
                    USD = currencyRates.Rates[GetAvailableCurrencies[i]];
                    oldUSD = differenceCurrencyRates.Rates[GetAvailableCurrencies[i]];
                }
            }

            for (int i = 0; i < currenciesSymbols.Length; i++)
            {
                var symbol = currenciesSymbols[i];
                var current = USD / currencyRates.Rates[currenciesSymbols[i]];
                var difference = oldUSD / differenceCurrencyRates.Rates[currenciesSymbols[i]];
                var percent = (current - difference) / difference * 100;

                currencies.Add(new Currency
                {
                    Base = "PLN",
                    Symbol = symbol,
                    Date = date,
                    Value = decimal.Round(current, 6),
                    ValueSevenDaysEarlier = decimal.Round(difference, 6),
                    Provider = ProviderName
                });
            }
            return currencies;
        }

        public override decimal Convert(string from, string to, decimal amount, DateTime date)
        {
                CurrencyRates currencyRates = OpenExchangeRates.getHistoricalExchange(date);

                List<Currency> currencies = new List<Currency>();

                decimal amountTo = 0;
                decimal amountFrom = 0;

                for (int i = 0; i < GetAvailableCurrencies.Length; i++)
                {
                    var symbol = GetAvailableCurrencies[i];

                    if (symbol == from)
                    {
                        amountFrom = currencyRates.Rates[GetAvailableCurrencies[i]];
                    }

                    if (symbol == to)
                    {
                        amountTo = currencyRates.Rates[GetAvailableCurrencies[i]];
                    }

                }

                return (amountTo / amountFrom) * amount;   
        }
    }
    
}
