﻿using System;
using CurrencyExchange.Data;
using Microsoft.AspNetCore.Http;

namespace CurrencyExchange.Services.ExchangeRateProviders
{
    public class ExchangeRateProviderFactory
    {
        private readonly DatabaseContext _databaseContext;

        public ExchangeRateProviderFactory(DatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }
        public ExchangeRateProvider CreateExchangeRateProvider(IHttpContextAccessor httpContextAccessor)
        {
            ExchangeRateProvider exchangeRateProvider = null;
            var providerName = httpContextAccessor.HttpContext.Session.GetString("Provider");
            switch (providerName)
            {
               case ("OpenExchangeRates"):
                   exchangeRateProvider = new OpenExchangeRatesAdapter();
                    break;
                default:
                    exchangeRateProvider = new FixerAdapter();
                    break;
            }
            return new ExchangeRateProviderProxy(exchangeRateProvider, _databaseContext);
        }
    }
}
