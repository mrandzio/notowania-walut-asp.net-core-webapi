﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using CurrencyExchange.Models;

namespace CurrencyExchange.Services.ExchangeRateProviders
{
    public class OpenExchangeRates
    {
        static string ID = "265723ad50ff4837976c550bee7c3e9c";

        public static CurrencyRates getHistoricalExchange (DateTime date)
        {
            string url = "https://openexchangerates.org/api/historical/" + date.ToString("yyyy-MM-dd") + ".json?app_id="+ ID;
            return _download_serialized_json_data<CurrencyRates>(url);
        }

        public static CurrencyRates getConvertCurrency(string from, DateTime date)
        {
            string url = "https://openexchangerates.org/api/historical/" + date.ToString("yyyy-MM-dd") + ".json?app_id=" + ID + "&base=" + from;
            return _download_serialized_json_data<CurrencyRates>(url);
        }

        public static decimal Convert(string from, string to, decimal value)
        {
            string url = "https://openexchangerates.org/api/convert/"+ value.ToString() +"/" + from + "/" + to + "?app_id=" + ID;
            return _download_serialized_json_data<ConvertOER>(url).Response;
        }

        public static CurrencyRatesTimeSeries getTimeSeriesExchange(string[] symbols, DateTime startDate, DateTime endDate)
        {
            var stringSymbol = string.Empty;
            for(int i = 0; i<symbols.Length; i++)
            {
                stringSymbol += symbols[i];
                if (i != symbols.Length - 1)
                    stringSymbol += ",";
            }
            string url = "https://openexchangerates.org/api/time-series.json?app_id=" + ID + "&base=PLN"
                + "&start="+ startDate.ToString("yyyy-MM-dd")
                +"&end=" + endDate.ToString("yyyy-MM-dd") 
                +"&base=PLN"
                +"&symbols=" + stringSymbol
                +"&prettyprint = 1";
            return _download_serialized_json_data<CurrencyRatesTimeSeries>(url);
        }

        private static T _download_serialized_json_data<T>(string url) where T : new()
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                try
                {
                    json_data = w.DownloadString(url);
                }
                catch (Exception) { }
                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
            }
        }
    }
}
