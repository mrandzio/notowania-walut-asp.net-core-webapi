﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CurrencyExchange.Data;
using CurrencyExchange.Models;
using Microsoft.EntityFrameworkCore;

namespace CurrencyExchange.Services.ExchangeRateProviders
{
    public class ExchangeRateProviderProxy : ExchangeRateProvider
    {
        private readonly ExchangeRateProvider _exchangeRateProvider;
        private readonly DatabaseContext _dbContext;

        public ExchangeRateProviderProxy(ExchangeRateProvider exchangeRateProvider, DatabaseContext dbContext)
        {
            _exchangeRateProvider = exchangeRateProvider;
            _dbContext = dbContext;
        }

        public override string ProviderName => _exchangeRateProvider.ProviderName;

        public override Currency GetCurrency(string currencyString, DateTime date)
        {
            Currency currency;
            if (IsCurrencyInDatabase(currencyString, date))
            {
                currency = _dbContext.Currencies.First(c => c.Symbol == currencyString && c.Base == BaseCurrencySymbol && c.Date == date.Date &&
                    c.Provider == ProviderName);
            }
            else
            {
                currency = _exchangeRateProvider.GetCurrency(currencyString, date.Date);
                _dbContext.Currencies.Add(currency);
                _dbContext.SaveChanges();
            }

            return currency;
        }

        public override IEnumerable<Currency> GetCurrenciesByDate(string[] currenciesSymbols, DateTime date)
        {
            var currenciesSymbolsNotInDatabase = new List<string>();
            var currenciesSymbolsInDatabase = new List<string>();

            foreach (var currencySymbol in currenciesSymbols)
            {
                if(IsCurrencyInDatabase(currencySymbol, date))
                    currenciesSymbolsInDatabase.Add(currencySymbol);
                else
                    currenciesSymbolsNotInDatabase.Add(currencySymbol);
            }

            var currenciesFromApi = GetCurrenciesFromApiAndSaveToDatabase(currenciesSymbolsNotInDatabase.ToArray(), date);

            var currenciesFromDatabase = _dbContext.Currencies.Where(c =>
                currenciesSymbolsInDatabase.Contains(c.Symbol) && c.Base == BaseCurrencySymbol && c.Date.Date == date.Date &&
                c.Provider == ProviderName).ToList();

            if(currenciesFromApi != null) _dbContext.SaveChanges();

            return currenciesFromApi != null ? currenciesFromDatabase.Concat(currenciesFromApi) : currenciesFromDatabase;
        }

        public override decimal Convert(string from, string to, decimal amount, DateTime date) => _exchangeRateProvider.Convert(from, to, amount, date);

        private bool IsCurrencyInDatabase(string currencySymbol, DateTime date) => _dbContext.Currencies.Any(c => c.Symbol == currencySymbol && c.Base == BaseCurrencySymbol && c.Date.Date == date.Date && c.Provider == ProviderName);

        private List<Currency> GetCurrenciesFromApiAndSaveToDatabase(string[] currenciesSymbols, DateTime date)
        {
            if (currenciesSymbols.Length > 0)
            {
                var currenciesFromApi = _exchangeRateProvider.GetCurrenciesByDate(currenciesSymbols, date).ToList();
                _dbContext.AddRange(currenciesFromApi);
                return currenciesFromApi;
            }
            else
            {
                return null;
            }
        }
    }
}
