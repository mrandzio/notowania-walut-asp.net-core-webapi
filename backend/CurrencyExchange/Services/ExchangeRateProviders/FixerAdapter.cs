﻿using CurrencyExchange.Models;
using FixerIoCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CurrencyExchange.Services.ExchangeRateProviders
{
    public class FixerAdapter : ExchangeRateProvider
    {
        private Symbol BaseCurrencySymbolEnum => ConvertCurrencySymbolStringToEnumSymbol(BaseCurrencySymbol);
        private FixerIoClient _fixerIoClient;
        public override string ProviderName { get; } = "fixer.io";

        public override Currency GetCurrency(string currencyString, DateTime date)
        {
            var currencySymbol = (Symbol)Enum.Parse(typeof(Symbol), currencyString);
            _fixerIoClient =  new FixerIoClient();

            var value = 1/_fixerIoClient.Convert(BaseCurrencySymbolEnum, currencySymbol, date);
            var valueSevenDaysEarlier = 1/_fixerIoClient.Convert(BaseCurrencySymbolEnum, currencySymbol, date.AddDays(-7));

            return new Currency
            {
                Base = BaseCurrencySymbol,
                Symbol = currencyString,
                Date = date.Date,
                Value = decimal.Round(value, 6),
                ValueSevenDaysEarlier = decimal.Round(valueSevenDaysEarlier, 6),
                Provider = ProviderName
            };
        }

        public override IEnumerable<Currency> GetCurrenciesByDate(string[] currenciesSymbols, DateTime date)
        {
            var symbols = GetSymbols(currenciesSymbols);
            _fixerIoClient = new FixerIoClient(BaseCurrencySymbolEnum, symbols);
            var quote = _fixerIoClient.GetForDate(date);
            var quoteSevenDaysEarlier = _fixerIoClient.GetForDate(date.AddDays(-7));
            var currencies = new List<Currency>();

            foreach (var rate in quote.Rates)
            {
                var value = 1/rate.Value;
                var valueSevenDaysEarlier = 1/quoteSevenDaysEarlier.Rates[rate.Key];
                currencies.Add(new Currency
                {
                    Base = BaseCurrencySymbol,
                    Symbol = rate.Key,
                    Date = date.Date,
                    Value = decimal.Round(value, 6),
                    ValueSevenDaysEarlier = decimal.Round(valueSevenDaysEarlier, 6),
                    Provider = ProviderName
                });
            }

            return currencies;
        }

        public override decimal Convert(string from, string to, decimal amount, DateTime date)
        {
            _fixerIoClient = new FixerIoClient();
            var value = _fixerIoClient.Convert(ConvertCurrencySymbolStringToEnumSymbol(from),
                ConvertCurrencySymbolStringToEnumSymbol(to), date);
            return decimal.Round(value*amount, 6);
        }

        private Symbol ConvertCurrencySymbolStringToEnumSymbol(string currencySymbolString)
        {
            return (Symbol) Enum.Parse(typeof(Symbol), currencySymbolString);
        }

        private List<Symbol> GetSymbols(string[] currenciesSymbolStrings)
        {
            return currenciesSymbolStrings.Select(currencySymbolString => (Symbol) Enum.Parse(typeof(Symbol), currencySymbolString)).ToList();
        }
    }
}
