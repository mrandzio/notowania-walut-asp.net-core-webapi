﻿using CurrencyExchange.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CurrencyExchange.Services.ExchangeRateProviders
{
    public abstract class ExchangeRateProvider
    {
        public abstract string ProviderName { get; }
        public abstract Currency GetCurrency(string currencyString, DateTime date);
        public abstract IEnumerable<Currency> GetCurrenciesByDate(string[] currenciesSymbols, DateTime date);
        public abstract decimal Convert(string from, string to, decimal amount, DateTime date);

        public string BaseCurrencySymbol { get; set; } = "PLN";

        public IEnumerable<Currency> GetCurrencyByDateRange(string symbol, DateTime startDate, DateTime endDate)
        {
            if (startDate.Date > endDate.Date) ReplaceDates(startDate, endDate);

            var currencies = new List<Currency>();

            while (startDate.Date != endDate.AddDays(1).Date)
            {
                currencies.Add(GetCurrency(symbol, startDate));
                startDate = startDate.AddDays(1);
            }

            return currencies;

        }

        public IDictionary<DateTime, IEnumerable<Currency>> GetCurrenciesByDateRange(string[] symbols, DateTime startDate,
            DateTime endDate)
        {
            if(startDate.Date > endDate.Date) ReplaceDates(startDate, endDate);

            var currencies = new Dictionary<DateTime, IEnumerable<Currency>>();

            while (startDate.Date != endDate.AddDays(1).Date)
            {
                currencies.Add(startDate, GetCurrenciesByDate(symbols, startDate));
                startDate = startDate.AddDays(1);
            }

            return currencies;
        }

        public IDictionary<string, string> GetCurrenciesNames => new Dictionary<string, string>
        {
            { "USD", "Dolar amerykański" },
            { "EUR", "Euro" },
            { "JPY","Jen japoński" },
            { "GBP","Funt szterling" },
            { "AUD","Dolar australijski" },
            { "CHF","Frank szwajcarski" },
            { "CAD","Dolar kanadyjski" },
            { "HKD","Dolar hongkoński" },
            { "SEK","Korona szwedzka" },
            { "NZD","Dolar nowozelandzki" },
            { "CNY","Renminbi" },
            { "RUB","Rubel rosyjski" },
            { "HUF","Forint węgierski" },
            { "CZK","Korona czeska" },
            { "NOK","Korona norweska" },
            { "DKK","Korona duńska" },
            { "PLN","Polski złoty" }
        };

        public string[] GetAvailableCurrencies => new string[]
        {
            "USD",
            "EUR",
            "JPY",
            "GBP",
            "AUD",
            "CHF",
            "CAD",
            "HKD",
            "SEK",
            "NZD",
            "CNY",
            "RUB",
            "HUF",
            "CZK",
            "NOK",
            "DKK",
            "PLN"
        };

        private void ReplaceDates(DateTime firstDate, DateTime secondDate)
        {
            var tempDate = firstDate;
            firstDate = secondDate;
            secondDate = firstDate;
        }
    }
}